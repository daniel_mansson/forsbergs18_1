﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateButton : MonoBehaviour
{
	public Vector2 m_focusSize;
	public Vector2 m_unfocusSize;
	public UiController.State m_state;

	public event System.Action<StateButton, UiController.State> ButtonClicked;

	private Button m_button;
	private bool m_focusState;

	public void SetFocus(bool focus)
	{
		m_focusState = focus;
	}

	void Start ()
	{
		m_button = GetComponent<Button>();
		m_button.onClick.AddListener(OnClick);
	}

	private void OnClick()
	{
		if (ButtonClicked != null)
			ButtonClicked(this, m_state);
	}

	void Update ()
	{
		RectTransform t = (RectTransform)m_button.transform;

		var s = t.sizeDelta;
		s = Vector2.Lerp(s, m_focusState ? m_focusSize : m_unfocusSize, Time.deltaTime * 9f);
		t.sizeDelta = s;
	}
}
