﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PersonController
{
	public PersonModel PersonModel { get; private set; }

	private StorageService m_storageService;
	ItemDirectory m_itemDirectory;
	public bool m_dirty = false;

	public PersonController(StorageService storageService, ItemDirectory itemDict)
	{
		m_storageService = storageService;
		m_itemDirectory = itemDict;

		//For the final presentation, we wanted to reset the player every startup.
		//PersonModel = m_storageService.Load<PersonModel>("localperson");
		if (PersonModel == null)
		{
			PersonModel = new PersonModel();
			Save();
		}
	}

	void Save()
	{
		m_storageService.Save("localperson", PersonModel);
		m_dirty = true;
	}

	public bool IsEquipped(string item)
	{
		return PersonModel.items.Contains(item);
	}

	public void Equip(string item)
	{
		var type = m_itemDirectory.GetItemInfo(item).type;

		PersonModel.items.RemoveAll(i => m_itemDirectory.GetItemInfo(i).type == type);
		PersonModel.items.Add(item);

		PersonModel.OnChanged();
		Save();
	}

	public void Unequip(string type)
	{
		PersonModel.items.RemoveAll(i => m_itemDirectory.GetItemInfo(i).type == type);

		PersonModel.OnChanged();
		Save();
	}
}
