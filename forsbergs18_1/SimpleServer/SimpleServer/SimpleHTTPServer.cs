﻿// MIT License - Copyright (c) 2016 Can Güney Aksakalli
// https://aksakalli.github.io/2014/02/24/simple-http-server-with-csparp.html

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Newtonsoft.Json;

class SimpleHTTPServer
{
	private Thread _serverThread;
	private string _rootDirectory;
	private HttpListener _listener;
	private int _port;

	[System.Serializable]
	public class PlayerEntry
	{
		public string id;
		public int views;
		public int upvotes;
		public int downvotes;
	}

	[System.Serializable]
	public class PersonEntry
	{
		public string id;
		public string data;
	}

	[System.Serializable]
	public class IdRequest
	{
		public string id;
	}

	[System.Serializable]
	public class RateRequest
	{
		public string upvote;
		public string downvote;
	}

	[System.Serializable]
	public class PairResponse
	{
		public PersonEntry person1;
		public PersonEntry person2;
	}

	public static Dictionary<string, PlayerEntry> s_players = new Dictionary<string, PlayerEntry>();

	public static List<PersonEntry> s_persons = new List<PersonEntry>();

	public int Port
	{
		get { return _port; }
		private set { }
	}

	/// <summary>
	/// Construct server with given port.
	/// </summary>
	/// <param name="path">Directory path to serve.</param>
	/// <param name="port">Port of the server.</param>
	public SimpleHTTPServer(string path, int port)
	{
		this.Initialize(path, port);
	}

	/// <summary>
	/// Construct server with suitable port.
	/// </summary>
	/// <param name="path">Directory path to serve.</param>
	public SimpleHTTPServer(string path)
	{
		//get an empty port
		TcpListener l = new TcpListener(IPAddress.Loopback, 0);
		l.Start();
		int port = ((IPEndPoint)l.LocalEndpoint).Port;
		l.Stop();
		this.Initialize(path, port);
	}

	/// <summary>
	/// Stop server and dispose all functions.
	/// </summary>
	public void Stop()
	{
		_serverThread.Abort();
		_listener.Stop();
	}

	private void Listen()
	{
		_listener = new HttpListener();
		_listener.Prefixes.Add("http://*:" + _port.ToString() + "/");
		_listener.Start();
		while (true)
		{
			try
			{
				HttpListenerContext context = _listener.GetContext();
				Process(context);
			}
			catch (Exception ex)
			{

			}
		}
	}

	public static string StreamToString(Stream stream)
	{
		using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
		{
			return reader.ReadToEnd();
		}
	}

	public static Stream StringToStream(string src)
	{
		byte[] byteArray = Encoding.UTF8.GetBytes(src);
		return new MemoryStream(byteArray);
	}

	private void Process(HttpListenerContext context)
	{
		try
		{
			string path = context.Request.Url.AbsolutePath;
			Console.WriteLine(path);
			path = path.Substring(1);

			var body = StreamToString(context.Request.InputStream);
			Console.WriteLine(body);

			switch (path)
			{
				case "upload":
					UploadPerson(body, context);
					break;
				case "getstats":
					GetStats(body, context);
					break;
				case "getpair":
					GetPair(body, context);
					break;
				case "rate":
					Rate(body, context);
					break;
				default:
					context.Response.StatusCode = (int)HttpStatusCode.NotFound;
					break;
			}
			
		}
		catch (Exception ex)
		{
			Console.WriteLine("Error: " + ex.Message);
			context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
		}

		Console.WriteLine("Players: " + s_players.Count + "   Persons: " + s_persons.Count);

		context.Response.OutputStream.Close();
	}

	private void Rate(string body, HttpListenerContext context)
	{
		var req = JsonConvert.DeserializeObject<RateRequest>(body);

		var up = GetOrCreatePlayer(req.upvote);
		up.upvotes++;
		var down = GetOrCreatePlayer(req.downvote);
		down.downvotes++;

		var data = "{}";

		context.Response.ContentType = "application/json";

		byte[] buffer = Encoding.UTF8.GetBytes(data);
		int nbytes = buffer.Length;
		context.Response.OutputStream.Write(buffer, 0, nbytes);

		context.Response.ContentLength64 = buffer.Length;

		context.Response.StatusCode = (int)HttpStatusCode.OK;
		context.Response.OutputStream.Flush();
	}

	PlayerEntry GetOrCreatePlayer(string id)
	{
		PlayerEntry e;
		if (!s_players.TryGetValue(id, out e))
		{
			e = new PlayerEntry()
			{
				id = id
			};
			s_players.Add(id, e);
		}

		return e;
	}

	static Random s_random = new Random();

	private void GetPair(string body, HttpListenerContext context)
	{
		var req = JsonConvert.DeserializeObject<IdRequest>(body);
		var player = GetOrCreatePlayer(req.id);

		var ok = s_persons.Where(p => p.id != player.id).ToList();
		PersonEntry p1;
		PersonEntry p2;

		if (ok.Count > 0)
		{
			int i = s_random.Next() % ok.Count;
			p1 = ok[i];
			ok.RemoveAt(i);

			var stats = GetOrCreatePlayer(p1.id);
			stats.views++;
		}
		else
		{
			p1 = new PersonEntry()
			{
				id = "mock1",
				data = "{}"
			};
		}

		if (ok.Count > 0)
		{
			int i = s_random.Next() % ok.Count;
			p2 = ok[i];
			ok.RemoveAt(i);

			var stats = GetOrCreatePlayer(p2.id);
			stats.views++;
		}
		else
		{
			p2 = new PersonEntry()
			{
				id = "mock2",
				data = "{}"
			};
		}

		var resp = new PairResponse()
		{
			person1 = p1,
			person2 = p2
		};

		var data = JsonConvert.SerializeObject(resp);

		context.Response.ContentType = "application/json";

		byte[] buffer = Encoding.UTF8.GetBytes(data);
		int nbytes = buffer.Length;
		context.Response.ContentLength64 = buffer.Length;
		context.Response.OutputStream.Write(buffer, 0, nbytes);

		context.Response.StatusCode = (int)HttpStatusCode.OK;
		context.Response.OutputStream.Flush();
	}

	private void GetStats(string body, HttpListenerContext context)
	{
		var req = JsonConvert.DeserializeObject<IdRequest>(body);
		var player = GetOrCreatePlayer(req.id);
		var data = JsonConvert.SerializeObject(player);

		context.Response.ContentType = "application/json";

		byte[] buffer = Encoding.UTF8.GetBytes(data);
		int nbytes = buffer.Length;
		context.Response.ContentLength64 = buffer.Length;

		context.Response.OutputStream.Write(buffer, 0, nbytes);

		context.Response.StatusCode = (int)HttpStatusCode.OK;
		context.Response.OutputStream.Flush();
	}

	private void UploadPerson(string body, HttpListenerContext context)
	{
		var req = JsonConvert.DeserializeObject<PersonEntry>(body);
		var player = GetOrCreatePlayer(req.id);

		var old = s_persons.FirstOrDefault(p => p.id == req.id);
		if (old != null)
		{
			s_persons.Remove(old);
		}
		s_persons.Add(req);

		if (s_persons.Count > 1000)
			s_persons.RemoveAt(0);

		context.Response.ContentType = "application/json";

		byte[] buffer = Encoding.UTF8.GetBytes("{}");
		int nbytes = buffer.Length;
		context.Response.ContentLength64 = buffer.Length;
		context.Response.OutputStream.Write(buffer, 0, nbytes);

		context.Response.StatusCode = (int)HttpStatusCode.OK;
		context.Response.OutputStream.Flush();
	}

	private void Initialize(string path, int port)
	{
		this._rootDirectory = path;
		this._port = port;
		_serverThread = new Thread(this.Listen);
		_serverThread.Start();
	}


}