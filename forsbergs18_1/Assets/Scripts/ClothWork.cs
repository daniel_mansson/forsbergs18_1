﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothWork : MonoBehaviour
{
	public PersonVisual m_visual;

	// Use this for initialization
	IEnumerator Start ()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;

		m_visual.SetModel(Game.Instance.PersonController.PersonModel);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	Vector2 m_scroll;

	private void OnGUI()
	{
		if (Game.Instance != null && Game.Instance.ItemDirectory != null)
		{
			m_scroll = GUILayout.BeginScrollView(m_scroll, GUILayout.Width(300));

			var pc = Game.Instance.PersonController;
			foreach (var item in Game.Instance.ItemDirectory.m_infoLookup.Values)
			{
				var isEq = pc.IsEquipped(item.id);

				if (GUILayout.Button((isEq ? "- " : "") + item.id))
				{
					if (isEq)
					{
						pc.Unequip(item.type);
					}
					else
					{
						pc.Equip(item.id);
					}
				}
			}

			GUILayout.EndScrollView();
		}
	}
}
