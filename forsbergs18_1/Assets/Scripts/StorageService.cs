﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageService
{
	public void SaveLocal(string key, string data)
	{
		PlayerPrefs.SetString(key, data);
		PlayerPrefs.Save();
	}

	public string LoadLocal(string key)
	{
		return PlayerPrefs.GetString(key, "");
	}

	public T Load<T>(string key)
	{
		var json = LoadLocal(key);
		return JsonUtility.FromJson<T>(json);
	}

	public void Save<T>(string key, T obj)
	{
		var json = JsonUtility.ToJson(obj);
		SaveLocal(key, json);
	}
}
