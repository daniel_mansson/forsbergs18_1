﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryModel
{
	public List<string> items = new List<string>();
	public int money = 0;

	public event System.Action<InventoryModel> Changed;

	public void OnChanged()
	{
		if (Changed != null)
			Changed(this);
	}
}
