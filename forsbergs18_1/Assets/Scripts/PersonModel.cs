﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PersonModel
{
	public string name = "Noname";
	public List<string> items = new List<string>();

	public event System.Action<PersonModel> Changed;

	public void OnChanged()
	{
		if (Changed != null)
			Changed(this);
	}
}
