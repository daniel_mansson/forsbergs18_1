﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class OnlineService
{
	[System.Serializable]
	public class PlayerEntry
	{
		public string id;
		public int views;
		public int upvotes;
		public int downvotes;
	}

	[System.Serializable]
	public class PersonEntry
	{
		public string id;
		public string data;
	}

	[System.Serializable]
	public class IdRequest
	{
		public string id;
	}

	[System.Serializable]
	public class RateRequest
	{
		public string upvote;
		public string downvote;
	}

	[System.Serializable]
	public class PairResponse
	{
		public PersonEntry person1;
		public PersonEntry person2;
	}

	public string m_playerId;
	private static readonly string s_baseurl = "http://34.244.4.24:5055";

	public void Init()
	{
		m_playerId = Guid.NewGuid().ToString();
		//todo save
	}

	public void GetStats(Action onDone)
	{

	}

	public void Rate(RateRequest r, Action onDone)
	{

	}

	public void GetPair(Action<PairResponse> onDone)
	{
		var r = new IdRequest()
		{
			id = m_playerId
		};

		var body = JsonUtility.ToJson(r);
		Post(s_baseurl + "/getpair", body, res =>
		{
			if (res != null)
			{
				Debug.Log("Got pair: " + res);
				var pair = JsonUtility.FromJson<PairResponse>(res);
				onDone(pair);
			}
			else
			{
				onDone(null);
			}
		});
	}

	public void Upload(PersonModel model)
	{
		var data = JsonUtility.ToJson(model);
		var r = new PersonEntry()
		{
			id = m_playerId,
			data = data
		};

		var body = JsonUtility.ToJson(r);
		Post(s_baseurl+ "/upload", body, res => 
		{
			Debug.Log(res);
		});
	}

	class PostState
	{
		public HttpWebRequest request;
		public Action<string> onDone;
	}

	private void Post(string url, string body, Action<string> onDone = null)
	{
		try
		{
			var request = (HttpWebRequest)WebRequest.Create(url);

			var data = Encoding.UTF8.GetBytes(body);

			request.Method = "POST";
			request.ContentType = "application/json";
			request.ContentLength = data.Length;

			using (var stream = request.GetRequestStream())
			{
				stream.Write(data, 0, data.Length);
			}

			request.BeginGetResponse(PostCallback, new PostState()
			{
				request = request,
				onDone = onDone
			});
		}
		catch (Exception e)
		{
			Debug.LogWarning(e.Message);
			if (onDone != null)
			{
				onDone(null);
			}
		}
	}

	private static string StreamToString(Stream stream)
	{
		using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
		{
			return reader.ReadToEnd();
		}
	}

	private void PostCallback(IAsyncResult ar)
	{
		var state = (PostState)ar.AsyncState;
		var response = (HttpWebResponse) state.request.EndGetResponse(ar);

		if (state.onDone != null)
		{
			if (ar.IsCompleted)
			{
				var stream = response.GetResponseStream();
				var data = StreamToString(stream);
				state.onDone(data);
			}
			else
			{
				state.onDone(null);
			}
		}
	}
}
