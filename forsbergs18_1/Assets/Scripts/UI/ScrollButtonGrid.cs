﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollButtonGrid : MonoBehaviour
{
	public class Entry
	{
		public string id;
		public bool selected;
		public System.Action<Entry, bool> onSelectedChanged;
	}

	public ItemButton m_buttonPrefab;
	public GameObject m_content;

	public float m_visibleHeight = 150f;
	public float m_hiddenHeight = -0.3f;
	public bool HasBeenInitialized { get; private set; }

	private List<ItemButton> m_buttons = new List<ItemButton>();
	private bool m_visible;
	private RectTransform m_transform;

	public void Init (List<Entry> entries)
	{
		foreach (var button in m_buttons)
		{
			Destroy(button.gameObject);
		}
		m_buttons.Clear();

		foreach (var entry in entries)
		{
			var button = Instantiate(m_buttonPrefab, m_content.transform);
			button.Init(entry);
			m_buttons.Add(button);
		}

		HasBeenInitialized = true;
	}

	public void SetVisible(bool visible)
	{
		m_visible = visible;
	}

	private void Start()
	{
		m_transform = (RectTransform)transform;
	}

	void Update ()
	{
		var s = m_transform.sizeDelta;
		s.y = Mathf.Lerp(s.y, m_visible ? m_visibleHeight : m_hiddenHeight, Time.deltaTime * 14f);
		m_transform.sizeDelta = s;
	}
}
