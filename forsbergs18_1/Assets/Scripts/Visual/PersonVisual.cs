﻿using Spine;
using Spine.Unity;
using Spine.Unity.Modules.AttachmentTools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonVisual : MonoBehaviour
{
	PersonModel m_model;
	Skin m_customSkin;

	[SpineSkin]
	public string m_templateAttachmentsSkin = "base";
	public Material m_sourceMaterial;

	public bool m_local = true;

	void OnValidate()
	{
		if (m_sourceMaterial == null)
		{
			var skeletonAnimation = GetComponent<SkeletonAnimation>();
			if (skeletonAnimation != null)
				m_sourceMaterial = skeletonAnimation.SkeletonDataAsset.atlasAssets[0].materials[0];
		}
	}

	public void SetModel(PersonModel model)
	{
		if (m_model != null)
		{
			m_model.Changed -= OnModelChanged;
		}

		m_model = model;

		if (m_model != null)
		{
			m_model.Changed += OnModelChanged;
		}

		Refresh();
	}

	private void Start()
	{
		if (m_local)
			SetModel(Game.Instance.PersonController.PersonModel);
	}

	private void OnDestroy()
	{
		if (m_model != null)
		{
			m_model.Changed -= OnModelChanged;
		}
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			SetModel(m_model == null ? Game.Instance.PersonController.PersonModel : null);
		}

		if (m_model == null)
		{
			transform.localScale = Vector3.one * Mathf.Lerp(transform.localScale.x, 0f, Time.deltaTime * 5f);
		}
		else
		{
			transform.localScale = Vector3.one * Mathf.Lerp(transform.localScale.x, 1f, Time.deltaTime * 5f);
		}
	}

	private void OnModelChanged(PersonModel obj)
	{
		Refresh();
	}

	[ContextMenu("Refresh")]
	public void Refresh()
	{
		if (m_model != null)
		{
			var skeletonAnimation = GetComponent<SkeletonAnimation>();
			var skeleton = skeletonAnimation.Skeleton;

			m_customSkin = new Skin("custom skin");
			var templateSkin = skeleton.Data.FindSkin(m_templateAttachmentsSkin);

			foreach (var attach in skeleton.Skin.Attachments)
			{
				m_customSkin.SetAttachment(attach.Key.slotIndex, attach.Key.name, attach.Value);
			}

			var dir = Game.Instance.ItemDirectory;

			foreach (var item in m_model.items)
			{
				var info = dir.GetItemInfo(item);

				foreach (var entry in info.entries)
				{
					var sprite = Resources.Load<Sprite>(entry.sprite);
					if (sprite != null)
					{
						int slotIndex = skeleton.FindSlotIndex(entry.slot);
						Attachment templateAttachment = templateSkin.GetAttachment(slotIndex, entry.key);

						Attachment newAttachment = templateAttachment.GetRemappedClone(sprite, m_sourceMaterial);
						m_customSkin.SetAttachment(slotIndex, entry.key, newAttachment);
					}
				}
			}

			skeleton.SetSkin(m_customSkin);
			skeleton.SetSlotsToSetupPose();
			skeletonAnimation.Update(0);
		}
	}
}
