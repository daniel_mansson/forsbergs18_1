﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartWhenStartedFromEditor : MonoBehaviour {

	void Awake ()
	{
		if (Game.Instance == null)
		{
			SceneManager.LoadScene("Boot");
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}
