﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
	public UiController UiController;
	public PersonController PersonController { get; private set; }
	public InventoryController InventoryController { get; private set; }
	public OnlineService Online { get; private set; }
	public GachaController GachaController { get; private set; }
	public StorageService StorageService { get; private set; }
	public ItemDirectory ItemDirectory { get; private set; }

	public bool m_dontLoadScene;

	public static Game Instance
	{
		get; private set;
	}

	void Awake ()
	{
		Instance = this;
		GameObject.DontDestroyOnLoad(this.gameObject);
	}

	private void Start()
	{
		Init();
	}

	void Init()
	{
#if !UNITY_ANDROID && !UNITY_EDITOR
		Screen.SetResolution(600, 1000, false);
#endif

		StorageService = new StorageService();
		Online = new OnlineService();
		Online.Init();

		ItemDirectory = new ItemDirectory(StorageService);
		PersonController = new PersonController(StorageService, ItemDirectory);
		InventoryController = new InventoryController(StorageService, ItemDirectory);
		GachaController = new GachaController(InventoryController, ItemDirectory);

		foreach (var type in ItemDirectory.Categories)
		{
			var items = InventoryController.GetOwnedItemsOfType(type);
			if (items.Count > 0)
			{
				PersonController.Equip(items[Random.Range(0, items.Count)]);
			}
		}

		if(!m_dontLoadScene)
		SceneManager.LoadScene("Game");

		StartCoroutine(UploadPoll());
	}

	IEnumerator UploadPoll()
	{
		while (true)
		{
			while (!PersonController.m_dirty)
				yield return null;

			Debug.Log("Uploading");
			Game.Instance.Online.Upload(PersonController.PersonModel);
			PersonController.m_dirty = false;

			yield return new WaitForSeconds(5f);
		}
	}
}
