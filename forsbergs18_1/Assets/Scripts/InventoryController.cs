﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Service for a players currently owned items.
/// </summary>
public class InventoryController
{
	private StorageService m_storageService;
	private ItemDirectory m_itemDirectory;

	public InventoryModel InventoryModel { get; private set; }

	public InventoryController(StorageService storageService, ItemDirectory dir)
	{
		m_storageService = storageService;
		m_itemDirectory = dir;

		//For the final presentation, we wanted to reset the player every startup.
		//InventoryModel = m_storageService.Load<InventoryModel>("localinventory");
		if (InventoryModel == null)
		{
			InventoryModel = new InventoryModel();
			Save();
		}

		foreach (var item in dir.m_infoLookup.Values)
		{
			InventoryModel.items.Add(item.id);
		}
	}

	void Save()
	{
		m_storageService.Save("localinventory", InventoryModel);
	}

	public void AddItem(string item)
	{
		if (!InventoryModel.items.Any(i => item == i))
		{
			InventoryModel.items.Add(item);
			InventoryModel.OnChanged();
			Save();
		}
	}

	public List<string> GetOwnedItemsOfType(string type)
	{
		return InventoryModel.items.Where(i => m_itemDirectory.GetItemInfo(i).type == type).ToList();
	}

}
