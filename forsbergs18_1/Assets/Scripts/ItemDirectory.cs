﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemInfo
{
	[System.Serializable]
	public class OverrideEntry
	{
		public string slot;
		public string key;
		public string sprite;
	}

	public string id;
	public string type;
	public List<OverrideEntry> entries;
}

[System.Serializable]
public class ItemInfoData
{
	public List<ItemInfo> data = new List<ItemInfo>();
}

public class ItemDirectory
{
	public static List<string> Categories = new List<string>()
	{
		"hat",
		"hair",
		"eyes",
		"eyebrows",
		"nose",
		"mouth",
		"accessories",
		"torso",
		"legs",
		"shoes",
		"skin"
	};

	private StorageService m_storageService;

	public Dictionary<string, ItemInfo> m_infoLookup;

	public ItemDirectory(StorageService storageService)
	{
		this.m_storageService = storageService;

		var asset = Resources.Load<TextAsset>("itemdir");
		var json = "";
		if (asset != null)
			json = asset.text;

		m_infoLookup = new Dictionary<string, ItemInfo>();
		var infoContainer = JsonUtility.FromJson<ItemInfoData>(json);
		if (infoContainer == null)
			infoContainer = new ItemInfoData();

		foreach (var info in infoContainer.data)
		{
			m_infoLookup.Add(info.id, info);
		}
	}

	public ItemInfo GetItemInfo(string item)
	{
		return m_infoLookup[item];
	}
}
