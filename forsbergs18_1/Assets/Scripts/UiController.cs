﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
	public enum State
	{
		Gacha,
		Wardrobe,
		Online,
		Unknown
	}

	public List<float> m_cameraAngleTargets = new List<float>();
	public List<StateButton> m_stateButtons;

	public ScrollButtonGrid m_categories;
	public ScrollButtonGrid m_items;

	public Button m_leftVote;
	public Button m_rightVote;

	public PersonVisual m_pairVisual1;
	public PersonVisual m_pairVisual2;

	State m_state = State.Unknown;

	List<ScrollButtonGrid.Entry> m_categoryEntries = new List<ScrollButtonGrid.Entry>();
	string m_currentCategory = "";

	List<ScrollButtonGrid.Entry> m_itemEntries = new List<ScrollButtonGrid.Entry>();

	float m_cameraRotation = 0f;
	Camera m_camera;

	OnlineService.PairResponse m_currentPair;

	void Start()
	{
		m_camera = Camera.main;

		if (Game.Instance != null)
			Game.Instance.UiController = this;

		m_stateButtons[0].m_state = State.Gacha;
		m_stateButtons[1].m_state = State.Wardrobe;
		m_stateButtons[2].m_state = State.Online;

		foreach (var btn in m_stateButtons)
		{
			btn.ButtonClicked += StateButtonClicked;
		}

		SetState(State.Wardrobe);

		foreach (var category in ItemDirectory.Categories)
		{
			var entry = new ScrollButtonGrid.Entry()
			{
				id = category,
				selected = false,
				onSelectedChanged = OnCategorySelectionChanged
			};

			m_categoryEntries.Add(entry);
		}

		m_categories.Init(m_categoryEntries);

		m_leftVote.onClick.AddListener(OnLeftVote);
		m_rightVote.onClick.AddListener(OnRightVote);

		StartCoroutine(RefreshPair());
	}

	private void OnCategorySelectionChanged(ScrollButtonGrid.Entry entry, bool selected)
	{
		m_currentCategory = selected ? entry.id : "";
		m_items.SetVisible(m_currentCategory != "");

		foreach (var e in m_categoryEntries)
		{
			if (entry != e)
			{
				e.selected = false;
			}
		}

		ReinitItems();
	}

	private void StateButtonClicked(StateButton button, State state)
	{
		SetState(state);
	}

	void Update()
	{
		var target = m_cameraAngleTargets[(int)m_state];
		m_cameraRotation = Mathf.Lerp(m_cameraRotation, target, Time.deltaTime * 5f);

		var rot = m_camera.transform.localEulerAngles;
		rot.y = m_cameraRotation;
		m_camera.transform.rotation = Quaternion.Euler(rot);

		if (Input.GetKeyDown(KeyCode.Alpha1))
			SetState(State.Gacha);
		if (Input.GetKeyDown(KeyCode.Alpha2))
			SetState(State.Wardrobe);
		if (Input.GetKeyDown(KeyCode.Alpha3))
			SetState(State.Online);
	}

	public void SetState(State state)
	{
		if (m_state != state)
		{
			m_state = state;

			foreach (var btn in m_stateButtons)
			{
				btn.SetFocus(btn.m_state == m_state);
			}

			m_categories.SetVisible(m_state == State.Wardrobe);
			m_items.SetVisible(m_state == State.Wardrobe && m_currentCategory != "");
			ReinitItems();

			m_leftVote.gameObject.SetActive(m_state == State.Online);
			m_rightVote.gameObject.SetActive(m_state == State.Online);
		}
	}

	void OnLeftVote()
	{
		StartCoroutine(RefreshPair());
	}

	void OnRightVote()
	{
		StartCoroutine(RefreshPair());
	}

	bool m_refreshingPair = false;
	IEnumerator RefreshPair()
	{
		if (m_refreshingPair)
		{
			while (m_refreshingPair)
				yield return null;
		}
		else
		{
			m_refreshingPair = true;

			OnlineService.PairResponse pair = null;

			Game.Instance.Online.GetPair(resPair =>
			{
				pair = resPair;
				m_refreshingPair = false;
			});

			while (m_refreshingPair)
				yield return null;

			OnPairUpdated(pair);
		}
	}

	private void OnPairUpdated(OnlineService.PairResponse pair)
	{
		Debug.Log("updating");

		m_currentPair = pair;

		PersonModel p1 = null;
		PersonModel p2 = null;

		if (pair != null)
		{
			p1 = JsonUtility.FromJson<PersonModel>(pair.person1.data);
			p2 = JsonUtility.FromJson<PersonModel>(pair.person2.data);
		}

		m_pairVisual1.SetModel(p1);
		m_pairVisual2.SetModel(p2);
	}

	void ReinitItems()
	{
		if (m_currentCategory == "")
			return;

		List<string> allItems = Game.Instance.InventoryController.GetOwnedItemsOfType(m_currentCategory);
		m_itemEntries = new List<ScrollButtonGrid.Entry>();

		foreach (var item in allItems)
		{
			m_itemEntries.Add(new ScrollButtonGrid.Entry()
			{
				id = item,
				selected = Game.Instance.PersonController.IsEquipped(item),
				onSelectedChanged = OnItemSelectionChanged
			});
		}

		m_items.Init(m_itemEntries);
	}

	private void OnItemSelectionChanged(ScrollButtonGrid.Entry entry, bool selected)
	{
		if (!selected)
		{
			entry.selected = true;
		}
		else
		{
			Game.Instance.PersonController.Equip(entry.id);

			foreach (var e in m_itemEntries)
			{
				if (entry != e)
				{
					e.selected = false;
				}
			}
		}
	}
}
