﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
	public Image m_image;
	public Image m_selected;

	private Button m_button;
	private ScrollButtonGrid.Entry m_entry;

	public void Init(ScrollButtonGrid.Entry entry)
	{
		m_entry = entry;

		var sprite = Resources.Load<Sprite>("icon_" + entry.id);
		if (sprite == null)
		{
			sprite = Resources.Load<Sprite>("icon");
		}

		m_image.sprite = sprite;
		m_selected.gameObject.SetActive(m_entry.selected);
	}

	void Start ()
	{
		m_button = GetComponent<Button>();
		m_button.onClick.AddListener(OnClick);
	}

	private void OnClick()
	{
		m_entry.selected = !m_entry.selected;
		if (m_entry.onSelectedChanged != null)
		{
			m_entry.onSelectedChanged(m_entry, m_entry.selected);
		}
	}

	void Update ()
	{
		m_selected.gameObject.SetActive(m_entry.selected);
	}
}
